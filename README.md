# Processwire GeoInfo #

GeoInfo is a small module that Implements Geoplugin PHP web service. 
Please donate to "geoplugin.com" in order to keep the service alive.

The Module ads two methods to retrieve data from the web service.

## $page->GeoInfoIP('IP ADDRESS'); ##

you can enter the ip address manually. If left blank the server remote address will be used.
The retrieved data is then stored to the active session, in order to limit the requests.
The web service gives back following data.

'geoplugin_request'
'geoplugin_status'
'geoplugin_credit'
'geoplugin_city'
'geoplugin_region'
'geoplugin_areaCode'
'geoplugin_dmaCode'
'geoplugin_countryCode'
'geoplugin_countryName'
'geoplugin_continentCode'
'geoplugin_latitude'
'geoplugin_longitude'
'geoplugin_regionCode'
'geoplugin_regionName'
'geoplugin_currencyCode'
'geoplugin_currencySymbol'
'geoplugin_currencySymbol_UTF8'
'geoplugin_currencyConverter'

for e.g. $page->GeoInfoIP('IP ADDRESS')->geoplugin_countryCode; will give back the country iso code.

## $page->GeoInfoLatLong('LAT', 'LONG'); ##

Enter Latitude and Longitude in order to get following information:

'geoplugin_place'
'geoplugin_countryCode'
'geoplugin_region'
'geoplugin_regionAbbreviated'
'geoplugin_latitude'
'geoplugin_longitude'
'geoplugin_distanceMiles'
'geoplugin_distanceKilometers'

for e.g. $page->GeoInfoLatLong('40.712784', '-74.005941'); will give back the city name "New York City".

### Todo ###

- store places in json file / check if place exists
- implement nearby "service"


