<?php

    /**
     * ProcessWire 'GeoInfo' module
     *
     */
    class GeoInfo extends WireData implements Module {

        /**
         * getModuleInfo is a module required by all modules to tell ProcessWire about them
         *
         * @return array
         *
         */
        public static function getModuleInfo() {

            return array(
                'title' => 'GeoInfo Module',
                'version' => 100,
                'summary' => 'Implements Geoplugin PHP web service. Please donate to "geoplugin.com" in order to keep the service alive.',
                'href' => 'http://www.geoplugin.com/webservices/php',
                'singular' => true,
                'autoload' => true,
                'icon' => 'globe',
            );

        }

        /**
         * Initialize the module
         *
         */
        public function init() {

            # Geo Info based on IP
            $this->addHook('Page::GeoInfoIP', $this, 'GeoInfoIP');
            # Geo Info based on Latitude/Longitude
            $this->addHook('Page::GeoInfoLatLong', $this, 'GeoInfoLatLong');

        }

        /**
         * Get User IP Address
         *
         * @return string
         */
        public static function getUserIP() {

            if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER) && !empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',') > 0) {
                    $addr = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
                    return trim($addr[0]);
                } else {
                    return $_SERVER['HTTP_X_FORWARDED_FOR'];
                }
            } else {
                return $_SERVER['REMOTE_ADDR'];
            }

        }

        /**
         * Return Geo Info based on IP
         *
         * @param $event
         */
        public function GeoInfoIP($event) {

            $ip = $this->sanitizer->text($event->arguments[0]);

            # check if input is ip
            if (empty($ip) || !filter_var($ip, FILTER_VALIDATE_IP)) {
                $ip = GeoInfo::getUserIP();
            }

            # get session
            $session = $this->session->get('GeoInfoIP');

            if( empty($session) ){
                # query web service
                $result = (object)unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip=' . $ip));
                # write to session
                $this->session->set('GeoInfoIP', $result);
            }
            else {
                $result = $session;
            }

            $event->return = $result;

        }

        /**
         * Return Geo Info based on Latitude/Longitude
         *
         * @param $event
         */
        public function GeoInfoLatLong($event) {

            # get Latitude & Longitude
            $Lat = $this->sanitizer->text($event->arguments[0]);
            $Long = $this->sanitizer->text($event->arguments[1]);

            # get inputs
            $checkLat = preg_match('/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/', $Lat);
            $checkLong = preg_match('/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/', $Long);

            if($checkLat && $checkLong){
                $result = (object)unserialize(file_get_contents('http://www.geoplugin.net/extras/location.gp?lat='.$Lat.'&long='.$Long.'&format=php'));
                $event->return = $result;
            }

        }

    }
